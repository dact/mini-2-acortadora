from django.shortcuts import render, get_object_or_404
from django.urls import path
from django.http import HttpResponse, HttpResponseRedirect
from .models import Url
from django.views.decorators.csrf import csrf_exempt
from django.core.exceptions import ObjectDoesNotExist
from django.template import loader






@csrf_exempt
def formulario(request):
    if request.method == "GET":
        urls = Url.objects.all()
        url = ""
        allurls = ""
        pagina = ""
        template = loader.get_template('acorta/url.html')
        context = {
            'urls': urls,
        }
        response = template.render(context, request);
    elif request.method == "POST":
        key = request.POST['key']
        protocol = key.split("://")[0]
        if protocol == "https" or protocol == "http":
            url = Url(direction = key)
        else:
            contenido = "http://" + key
            url = Url(direction = contenido)
        try:
            url = Url.objects.get(direction = url.direction)
            response = "La URL original es : " + str(url.direction) + "<br>" + "La URL acortada es : " +  str(url.cutted)
        except Url.DoesNotExist:
            url.save()
            num = str(url.id)
            url.cutted = request.build_absolute_uri() + num
            url.save()
            template = loader.get_template('acorta/url2.html')
            context = {
                'url': url,
            }
            response = template.render(context, request);
    return HttpResponse(response)
def redireccion (request, acortada):
	url = get_object_or_404(Url, id=acortada)
	direc = url.direction
	return  HttpResponseRedirect(direc)
